export LC_ALL=en_US.UTF-8
export EDITOR=vim
export VISUAL=vim

export PATH=~/bin:$PATH
export GOOGLETEST_DIR=~/projects/googletest

# android dev
export ANDROID_SDK_ROOT=~/Library/Android/sdk
export ANDROID_TOOLCHAIN_ROOT=~/android/toolchain
export ANDROID_HOME=/Users/oleh/Library/Android/sdk
export ANDROID_ARCH=arm
export PATH=$PATH:/Users/oleh/Library/Android/sdk/platform-tools/:/Users/oleh/Library/Android/sdk/platform-tools


# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt append_history
setopt hist_ignore_space

DIRSTACKSIZE=30
setopt auto_pushd
setopt pushd_minus

bindkey -e

zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}' 'r:|[._-]=* r:|=*' '' 'l:|=* r:|=*'
zstyle ':completion:*' menu select
zstyle :compinstall filename '/Users/oleh/.zshrc'

autoload -Uz compinit
compinit

########## aliases ##########
# Some shortcuts for different directory listings
alias ls='ls -Gh'                 # classify files in colour

########## aliases end ##########

# make ^W delete to nearest slash
# https://unix.stackexchange.com/a/250700/25880
# http://www.zsh.org/mla/users/2001/msg00870.html
my-backward-delete-word() {
    local WORDCHARS=${WORDCHARS/\//}
    zle backward-delete-word
}
zle -N my-backward-delete-word
bindkey '^W' my-backward-delete-word

######### prompt ##########
autoload -Uz vcs_info
vcs_info

precmd() {
	vcs_info
}

setopt prompt_subst

zstyle ':vsc_info:git:*' enable true
zstyle ':vcs_info:*' actionformats \
    '%F{yellow}[%F{green}%b%F{white}|%F{red}%a%F{yellow}]%f '
zstyle ':vcs_info:*' formats       \
    '%F{yellow}[%F{green}%b%F{yellow}]%f '

zstyle ':vcs_info:*' enable git

RPROMPT="$vcs_info_msg_0_"
PROMPT="%n@%m %F{blue}%1~%f%(!.#.$) "



export PATH="/usr/local/opt/curl/bin:$PATH"
